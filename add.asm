; Модуль с логикой добавления алгоритма
text segment public 'code'
assume cs: text

public addAlg

extrn selAlg:word
extrn checkAlg:proc, getAlg:proc

; Выполняет считывание нового алгортма и его добавление
addAlg proc
	push bx
	push cx
	
	mov bx, 0
	findCycle:
		; Поиск пустого алгоритма
		mov selAlg, bx
		call getAlg
		call checkAlg
		cmp cx, 1
		je newAlg
		cmp bx, 4; Если ни один алгоритм не свободен,
		je exitAdd;выйти
		
		inc bx
		cmp bx, 5
		jl findCycle
		jmp exitAdd
	
	newAlg:
		call readAlg
	
	exitAdd:
	pop cx
	pop bx
	ret
addAlg endp

; Производит считывание алгоритма
readAlg proc
	push bx
	push cx
	
	mov bx, 0
	readCycle:
		mov ah, 07h
		int 21h
		mov [si+bx], al
		mov ah, 02h
		mov dl, al
		int 21h
		inc bx
		cmp bx, 5
		jl readCycle
	
	pop cx
	pop bx
	ret
readAlg endp

text ends
end