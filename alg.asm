; Модуль с логикой визуализации выбранного алгоритма
text segment public 'code'
assume cs: text

; Общедоступные процедуры
public alg

; Подключение внешних переменных
extrn eSymbol:byte, x:word, y:word, color:byte, state:byte, nState:word
; Подключаемые подпрограммы из модуля options
extrn graphMode:proc, timer:proc, readKey:proc, getState:proc, getAlg:proc
extrn showAlg:proc

; Рисует квадрат
square proc
	push ax
	push bx
	push dx
	push cx

	mov ah, 0ch;	Функция вывода пикселя
	mov al, color;  Цвет пикселя
	mov bh, 0;  	Номер видеостраница
	mov dx, y
	mov bx, x
	mov cx, 50
	cycleSq:
		push cx
		mov cx, 50
		cycleSq2:
			push cx
			mov cx, bx
			int 10h
			inc bx
			pop cx
			loop cycleSq2
		inc dx
		mov bx, x
		pop cx
		loop cycleSq
	
	pop cx
	pop dx
	pop bx
	pop ax
	ret
square endp

; Рисует 5 квадратов с номерами от 1 до 5
range proc
	push di
	push si
	push bx
	push ax
	push dx
	push cx
	push x
	
	mov cx, 5
	mov ah, 02h; Установка позиции курсора/Вывод символа
	mov bh, 0;Видеостраница
	mov dh, 3;Строка
	mov dl, 6;Столбец
	mov di, dx; Курсор
	mov dl, '1'; Выводимый символ
	mov si, dx; Символ
	rCycle:
		call square
		
		mov dx, di
		int 10h;  Установка позиции курсора
		add dl, 13
		mov di, dx
		
		mov dx, si
		int 21h; Вывод символа на экран
		inc dl
		mov si, dx
		
		add x, 103
		inc dl
		loop rCycle
	
	pop x
	pop cx
	pop dx
	pop ax
	pop bx
	pop si
	pop di
	ret
range endp

selSquare proc
	push x
	push ax
	push bx
	push dx
	push cx
	
	; Вычисление координаты X в соответствии
	; с текущим состоянием
	mov al, 103
	mov ah, 0
	mov bh, 0
	mov bl, state
	sub bl, 49; В bl храниться номер состояния
	mul bl
	add x, ax
	mov cx, bx; Записать номер состояния в cx
	
	mov color, 4
	call square
	mov color, 14
	
	; Вычисление столбца для вывода номера
	mov al, 13
	mov ah, 0
	mul bl
	mov dl, al
	
	mov ah, 02h; Установка позиции курсора
	mov bh, 0;Видеостраница
	mov dh, 3;Строка
	add dl, 6;Столбец
	int 10h;  Установка позиции курсора
	
	mov ah, 02h; Установка позиции курсора/Вывод символа
	mov bx, cx
	add bl, 49
	mov dl, bl; Выводимый символ
	int 21h
	
	pop cx
	pop dx
	pop bx
	pop ax
	pop x
	ret
selSquare endp

alg proc
	push cx
	call showAlg
	call range
	call selSquare
	
	mov cx, 8
	algCycle:
		call timer
		call readKey
		
		cmp al, 'q'
		je algExit
		
		cmp eSymbol, 1
		je extendKey
		
		finishCycle:
		dec cx
		cmp cx, 0
		jne algCycle
		jmp algExit
	
	extendKey:
		mov eSymbol, 0
		cmp al, 75
		je lKey
		cmp al, 77
		je rKey
		jmp finishCycle
	
	lKey:
		mov cx, 8
		call leftKey
		jmp algCycle
	rKey:
		mov cx, 8
		call rightKey
		jmp algCycle
		
	algExit:
	pop cx
	ret
alg endp

; Обработка нажатия клавиши управления
; курсором "стрелка влево"
leftKey proc
	push cx
	cmp nState, 0
	je LK1
	jmp LK2
	
	LK1:
		mov nState, 4
		jmp exitLK
	LK2:
		dec nState
		
	exitLK:
	call range
	call getState
	call selSquare
	pop cx
	ret
leftKey endp

; Обработка нажатия клавиши управления
; курсором "стрелка вправо"
rightKey proc
	push cx
	cmp nState, 4
	je RK1
	jmp RK2
	
	RK1:
		mov nState, 0
		jmp exitRK
	RK2:
		inc nState
		
	exitRK:
	call range
	call getState
	call selSquare
	pop cx
	ret
rightKey endp

text ends
end