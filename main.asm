; Основной код программы
text segment public 'code'
assume cs: text, ds: data

; Определение общедоступных переменных
public x, y, fExit, eSymbol, color, state, nState, selAlg, algs
public showAlg

; Подключаемые подпрограммы из модуля options
extrn graphMode:proc, textMode:proc, timer:proc, readKey:proc
extrn getState:proc, checkAlg:proc, getAlg:proc
; Подключаемые подпрограммы из модуля alg
extrn alg:proc
; Подключаемые подпрограммы из модуля del
extrn delAlg:proc
; Подключаемые подпрограммы из модуля add
extrn addAlg:proc

; Вывод процедуры на экран
showAlg proc
	push ax
	push dx
	
	mov ax, 0900h
	mov dx, offset message
	int 21h
	
	add selAlg, 49
	mov ax, 0200h
	mov dl, byte ptr selAlg
	int 21h
	mov dl, 58
	int 21h
	mov dl, 32
	int 21h
	
	mov ax, 0900h
	mov dx, si
	int 21h
	
	mov ax, 0200h
	mov dl, 10
	int 21h
	mov dl, 13
	int 21h
	
	pop dx
	pop ax
	ret
showAlg endp

; Процеруда вывода всех алгоритмов на экран
listAlg proc
	mov cx, 0
	listCycle:
		mov selAlg, cx
		call getAlg
		
		push cx
		call checkAlg
		cmp cx, 1
		je finishCycle
		
		call showAlg
		finishCycle:
		pop cx
		inc cx
		cmp cx, 5
		jne listCycle
		
	ret
listAlg endp

begin:
	mov ax, data
	mov ds, ax
	
	call graphMode
	call listAlg
	
	cycleMain:
		call readKey
		
		cmp fExit, 1
		je exitMain
		
		cmp al, 'a'
		je addMain
		
		cmp al, 'd'
		je delMain
		
		cmp al, '0'
		ja callAlg
		
		jmp cycleMain
	
	callAlg:
		cmp al, '6'
		jnb cycleMain
		sub al, 49
		mov ah, 0
		mov selAlg, ax
		
		call getAlg
		call checkAlg
		cmp cx, 1
		je cycleMain
		
		call graphMode
		call getState
		call alg
		call graphMode
		call listAlg
		mov selAlg, 0
		mov nState, 0
		jmp cycleMain
	
	delMain:
		call delAlg
		call graphMode
		call listAlg
		jmp cycleMain
	
	addMain:
		call graphMode
		mov ax, 0900h
		mov dx, offset newAlg
		int 21h
		call addAlg
		call graphMode
		call listAlg
		jmp cycleMain
	
	exitMain:
	call textMode
	mov ah, 4ch; Выход из программы
	int 21h
text ends

data segment public
	algs dw alg1, alg2, alg3, alg4, alg5;Выбранный алгоритм
	x dw 50
	y dw 50
	selAlg dw 0;Номер выбранного алгоритма (0-4)
	nState dw 0;Номер состояния алгоритма (0-4)
	alg1 db '12345$'
	alg2 db '24135$'
	alg3 db 6 dup('$')
	alg4 db 6 dup('$')
	alg5 db 6 dup('$')
	color db 14
	fExit db 0
	eSymbol db 0; Расширенный символ: 0 - не расширенный, 1 - расширенный
	state db 0; Текущие состояние алгоритма
	message db 'Algoritm $'
	newAlg db 'New algoritm: $'
data ends

stk segment stack 'stack'
	dw 128 dup (0)
stk ends
end begin