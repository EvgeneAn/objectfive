; Модуль со вспомогательными подпрограммами
text segment public 'code'
assume cs: text

; Общедоступные процедуры
public graphMode, textMode, timer, readKey, getState, checkAlg, getAlg

; Подключение внешних переменных
extrn fExit:byte, eSymbol:byte, state:byte, nState:word, algs:word, selAlg:word

; Инициализирует графический режим
graphMode proc
	push ax
	push bx
	
    mov ah, 4fh; Функция, задающая графический режим
    mov al, 02h; Графический режим EGA
    mov bx, 103h;Режим с разрешением 800x600:256-цветов
    int 10h
	
	pop bx	
	pop ax
    ret
graphMode endp

; Инициализирует текстовый режим
textMode proc
	push ax
    mov ah, 00h;Функция задания режима
    mov al, 03h;Текстовый режим
    int 10h;    Вызов BIOS
	pop ax
    ret
textMode endp

; Задержка в одну секунду
timer proc
	push cx
	push bx
	push ax
	push dx
	
	mov ah, 00h
	int 1ah
	mov bx, dx
	timerCycle:
		int 1ah
		sub dx, bx
		cmp dx, 18
		jl timerCycle
	
	pop dx
	pop ax
	pop bx
	pop cx
	ret
timer endp

; Считывает символ с клавиатуры, если был введен
; символ F10, то в f_exit заносится 1
readKey proc
    push dx
	
    mov ah, 06h
    mov dl, 0ffh
    int 21h
    cmp al, 0;   если не расширенный символ,
    jne exitRead; выйти
    int 21h
	mov eSymbol, 1
    cmp al, 44h;  если не ввели F10,
    jne exitRead; выйти
    mov fExit, 1
    
    exitRead:
    pop dx
    ret
readKey endp

; Заносит адрес выбранного алгоритма в SI
getAlg proc
	push ax
	push bx
	
	mov al, 2; Для смещения по 2 байта
	mov bx, selAlg
	mov bh, 00h
	mul bl; Вычислить смещение с учетом номера алгоритма
	mov bx, ax; Занести в bx число для смещения
	mov si, algs+bx; Занести адрес алгоритма в si
	
	pop bx
	pop ax
	ret
getAlg endp

; Заносит в state выбранное состояние алгоритма
getState proc
	push bx
	push ax
	
	mov bx, nState
	mov al, [si+bx]; Инициализация состояния
	mov state, al;	 в соответствии с алгоритмом
	
	pop ax
	pop bx
	ret
getState endp

; Проверяет алгоритм на наличие ошибок
; Если в алгоритме есть недопустимые символы,
; то в cx записывается 1
checkAlg proc
	push bx
	
	mov cx, 0
	mov bx, 0
	cycleCheck:
		mov al, [si+bx]
		cmp al, '1'
		jl setCheck
		
		cmp al, '5'
		jg setCheck
		
		inc bx
		cmp bx, 5
		jge exitCheck
		jmp cycleCheck
	
	setCheck:
		mov cx, 1
	
	exitCheck:
	pop bx
	ret
checkAlg endp

text ends
end