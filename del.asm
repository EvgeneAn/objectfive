; Модуль с логикой удаления алгоритма.
text segment public 'code'
assume cs: text

public delAlg

extrn selAlg:word
extrn getAlg:proc, checkAlg:proc

; Удаляет последний корректный алгоритм
delAlg proc
	push cx
	push bx
	
	mov bx, 0
	delCycle:
		mov selAlg, bx
		call getAlg
		call checkAlg
		cmp cx, 1
		je remove
		cmp bx, 5
		je remove
		
		inc bx
		cmp bx, 5
		jle delCycle
		jmp delExit
	
	remove:
		cmp bx, 0
		je delExit
		
		dec bx
		mov selAlg, bx
		call getAlg
		mov bx, 0
		removeCycle:
			mov ah, 0
			mov al, '$'
			mov [si+bx], al
			inc bx
			cmp bx, 5
			jl removeCycle
	
	delExit:
	pop bx
	pop cx
	ret
delAlg endp

text ends
end